/*
 * DS3911 10-bit DAC
 * Copyright (C) 2017 Chronoptics
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

#include <asm/unaligned.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/regmap.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/sysfs.h>
#include <linux/uaccess.h>

#define DS3911_REG_CTRL   0x00
#define DS3911_REG_MODE   0x01
#define DS3911_REG_SRAM   0x02
#define DS3911_REG_TINDEX 0x03
#define DS3911_REG_TEMP   0x04
#define DS3911_REG_VCC    0x06
#define DS3911_REG_POR    0x78
#define DS3911_REG_OFFSET 0xF8
#define DS3911_REG_LUT    0x80

#define DS3911_DRV_NAME         "ds3911"
#define DS3911_MAX_DAC_CHANNELS 4
#define DS3911_LUT_SIZE         48
#define DS3911_OFFSET_SIZE      8
#define DS3911_MAX_SIZE         2

struct ds3911_dev {
  struct i2c_client *client;
  struct regmap *regmap;

  struct bin_attribute lut[DS3911_MAX_DAC_CHANNELS];
  struct bin_attribute offset[DS3911_MAX_DAC_CHANNELS];
  unsigned short vref_mv;
  unsigned short raw[DS3911_MAX_DAC_CHANNELS];
  unsigned short raw_max[DS3911_MAX_DAC_CHANNELS];  // The maximum raw value

  bool nSEE;     // Shadow eeprom.
  bool AEN;      // Automatic enable.
  bool SOFTTXD;  // Soft transmit disable.

  // Each dac
  unsigned short por[DS3911_MAX_DAC_CHANNELS];
  bool polarity[DS3911_MAX_DAC_CHANNELS];
  bool lut_enable[DS3911_MAX_DAC_CHANNELS];
  unsigned char tindex;
};

#define DS3911_CHANNEL(chan)                                                  \
  {                                                                           \
    .type = IIO_VOLTAGE, .indexed = 1, .output = 1, .channel = (chan),        \
    .datasheet_name = "CH" #chan,                                             \
    .info_mask_separate = BIT(IIO_CHAN_INFO_RAW) | BIT(IIO_CHAN_INFO_SCALE) | \
                          BIT(IIO_CHAN_INFO_ENABLE) |                         \
                          BIT(IIO_CHAN_INFO_CALIBBIAS) |                      \
                          BIT(IIO_CHAN_INFO_CALIBSCALE)                       \
  }

static const struct iio_chan_spec ds3911_channels[] = {
    DS3911_CHANNEL(0),
    DS3911_CHANNEL(1),
    DS3911_CHANNEL(2),
    DS3911_CHANNEL(3),
};

#define DS3911_MAX_REGISTER 0xFF

static const struct regmap_config ds3911_regmap_config = {
    .reg_bits = 8,
    .val_bits = 8,
    .max_register = DS3911_MAX_REGISTER,
};

// Select which table we are writing to
static int ds3911_set_table_select(struct ds3911_dev *dac, int channel) {
  u8 write_byte;
  if (channel < 0 || channel > 3) {
    return -1;
  }
  write_byte = channel + 4;
  return regmap_write(dac->regmap, DS3911_REG_CTRL, write_byte);
}

static ssize_t ds3911_read_file(struct file *fp, struct kobject *kobj,
                                struct bin_attribute *attr, char *buf,
                                loff_t offset, size_t count, int channel,
                                uint8_t reg_start) {
  // We set the channel
  ssize_t ret;
  struct ds3911_dev *dac;
  dac = (struct ds3911_dev *)attr->private;
  ret = ds3911_set_table_select(dac, channel);
  if (ret < 0) return ret;
  ret = regmap_bulk_read(dac->regmap, offset + reg_start, buf, count);
  return ret;
}

static ssize_t ds3911_write_file(struct file *fp, struct kobject *kobj,
                                 struct bin_attribute *attr, char *buf,
                                 loff_t offset, size_t count, int channel,
                                 uint8_t reg_start) {
  ssize_t ret;
  struct ds3911_dev *dac;
  dac = (struct ds3911_dev *)attr->private;
  ret = ds3911_set_table_select(dac, channel);
  if (ret < 0) return ret;
  ret = regmap_bulk_write(dac->regmap, offset + reg_start, buf, count);
  return ret;
}

static ssize_t ds3911_read_LUT_ch0(struct file *fp, struct kobject *kobj,
                                   struct bin_attribute *attr, char *buf,
                                   loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 0,
                          DS3911_REG_LUT);
}

static ssize_t ds3911_write_LUT_ch0(struct file *fp, struct kobject *kobj,
                                    struct bin_attribute *attr, char *buf,
                                    loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 0,
                           DS3911_REG_LUT);
}

static ssize_t ds3911_read_LUT_ch1(struct file *fp, struct kobject *kobj,
                                   struct bin_attribute *attr, char *buf,
                                   loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 1,
                          DS3911_REG_LUT);
}

static ssize_t ds3911_write_LUT_ch1(struct file *fp, struct kobject *kobj,
                                    struct bin_attribute *attr, char *buf,
                                    loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 1,
                           DS3911_REG_LUT);
}

static ssize_t ds3911_read_LUT_ch2(struct file *fp, struct kobject *kobj,
                                   struct bin_attribute *attr, char *buf,
                                   loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 2,
                          DS3911_REG_LUT);
}

static ssize_t ds3911_write_LUT_ch2(struct file *fp, struct kobject *kobj,
                                    struct bin_attribute *attr, char *buf,
                                    loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 2,
                           DS3911_REG_LUT);
}

static ssize_t ds3911_read_LUT_ch3(struct file *fp, struct kobject *kobj,
                                   struct bin_attribute *attr, char *buf,
                                   loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 3,
                          DS3911_REG_LUT);
}

static ssize_t ds3911_write_LUT_ch3(struct file *fp, struct kobject *kobj,
                                    struct bin_attribute *attr, char *buf,
                                    loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 3,
                           DS3911_REG_LUT);
}

static ssize_t ds3911_read_OFFSET_ch0(struct file *fp, struct kobject *kobj,
                                      struct bin_attribute *attr, char *buf,
                                      loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 0,
                          DS3911_REG_OFFSET);
}

static ssize_t ds3911_write_OFFSET_ch0(struct file *fp, struct kobject *kobj,
                                       struct bin_attribute *attr, char *buf,
                                       loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 0,
                           DS3911_REG_OFFSET);
}

static ssize_t ds3911_read_OFFSET_ch1(struct file *fp, struct kobject *kobj,
                                      struct bin_attribute *attr, char *buf,
                                      loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 1,
                          DS3911_REG_OFFSET);
}

static ssize_t ds3911_write_OFFSET_ch1(struct file *fp, struct kobject *kobj,
                                       struct bin_attribute *attr, char *buf,
                                       loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 1,
                           DS3911_REG_OFFSET);
}

static ssize_t ds3911_read_OFFSET_ch2(struct file *fp, struct kobject *kobj,
                                      struct bin_attribute *attr, char *buf,
                                      loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 2,
                          DS3911_REG_OFFSET);
}

static ssize_t ds3911_write_OFFSET_ch2(struct file *fp, struct kobject *kobj,
                                       struct bin_attribute *attr, char *buf,
                                       loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 2,
                           DS3911_REG_OFFSET);
}

static ssize_t ds3911_read_OFFSET_ch3(struct file *fp, struct kobject *kobj,
                                      struct bin_attribute *attr, char *buf,
                                      loff_t offset, size_t count) {
  return ds3911_read_file(fp, kobj, attr, buf, offset, count, 3,
                          DS3911_REG_OFFSET);
}

static ssize_t ds3911_write_OFFSET_ch3(struct file *fp, struct kobject *kobj,
                                       struct bin_attribute *attr, char *buf,
                                       loff_t offset, size_t count) {
  return ds3911_write_file(fp, kobj, attr, buf, offset, count, 3,
                           DS3911_REG_OFFSET);
}

static int ds3911_set_tindex(struct ds3911_dev *dac, unsigned char tindex) {
  int ret;
  ret = i2c_smbus_write_byte_data(dac->client, DS3911_REG_TINDEX, tindex);
  if (ret < 0) return ret;
  dac->tindex = tindex;
  return 0;
}

static int ds3911_get_tindex(struct ds3911_dev *dac) {
  int ret;
  ret = i2c_smbus_read_byte_data(dac->client, DS3911_REG_TINDEX);
  if (ret < 0) return ret;
  dac->tindex = ret;
  return 0;
}

static int ds3911_set_por(struct ds3911_dev *dac, int channel) {
  unsigned short por;
  unsigned char addr;
  addr = DS3911_REG_POR + 2 * channel;

  por = (dac->por[channel] << 6);
  if (dac->polarity[channel]) por = por | 0x0002;
  if (dac->lut_enable[channel]) por = por | 0x0001;

  return i2c_smbus_write_word_swapped(dac->client, addr, por);
}

static int ds3911_get_por(struct ds3911_dev *dac, int channel) {
  int ret;
  unsigned short por;
  unsigned char addr;
  addr = DS3911_REG_POR + 2 * channel;
  ret = i2c_smbus_read_word_swapped(dac->client, addr);
  if (ret < 0) return ret;
  por = ret;
  dac->polarity[channel] = (por & 0x0002) == 0 ? false : true;
  dac->lut_enable[channel] = (por & 0x0001) == 0 ? false : true;
  dac->por[channel] = por >> 6;
  return 0;
}

static int ds3911_set_mode(struct ds3911_dev *dac) {
  unsigned char mode = 0;
  if (dac->nSEE) mode = mode | 0x80;
  if (dac->AEN) mode = mode | 0x40;
  if (dac->SOFTTXD) mode = mode | 0x01;
  return i2c_smbus_write_byte_data(dac->client, DS3911_REG_MODE, mode);
}

static int ds3911_get_mode(struct ds3911_dev *dac) {
  int ret;
  unsigned char mode;
  ret = i2c_smbus_read_byte_data(dac->client, DS3911_REG_MODE);
  if (ret < 0) return ret;
  mode = ret;
  dac->nSEE = (mode & 0x80) == 0 ? false : true;
  dac->AEN = (mode & 0x40) == 0 ? false : true;
  dac->SOFTTXD = (mode & 0x01) == 0 ? false : true;
  return 0;
}

// The attribute of the Vcc of the DS3911 which is read-only
static ssize_t ds3911_show_vcc(struct device *dev,
                               struct device_attribute *attr, char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  ssize_t len = 0;
  int ret;
  int vcc_mv;
  u16 vcc_value;

  ret = i2c_smbus_read_word_swapped(dac->client, DS3911_REG_VCC);
  if (ret < 0) return ret;

  vcc_value = ret;
  vcc_value = vcc_value >> 3;

  // Full scale is 6.5536V, LSB of 800 micro Volts
  vcc_mv = vcc_value * 8;  // The voltage in milli-volts
  vcc_mv = vcc_mv / 10;    // Now in milli-volts and an integer

  len += scnprintf(buf + len, PAGE_SIZE - len, "%d", vcc_mv);
  buf[len - 1] = '\n';
  return len;
}

// Show the temperaure of the DS3911
static ssize_t ds3911_show_temperature(struct device *dev,
                                       struct device_attribute *attr,
                                       char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  ssize_t len = 0;
  int ret;
  u16 temp_value_u;
  s16 temp_value;
  int temp_c;  // Temperaure in mC

  ret = i2c_smbus_read_word_swapped(dac->client, DS3911_REG_TEMP);
  if (ret < 0) return ret;

  temp_value_u = ret;
  temp_value = (s16)temp_value_u;
  temp_value = temp_value / 16;

  // Convert the temperature value into C
  temp_c = temp_value * 125;
  len += scnprintf(buf + len, PAGE_SIZE - len, "%d", temp_c);
  buf[len - 1] = '\n';
  return len;
}

// The attribute of writing enable
static int ds3911_write_polarity(struct ds3911_dev *dac, bool polarity,
                                 int channel) {
  int ret;
  dac->polarity[channel] = polarity;
  ret = ds3911_set_por(dac, channel);
  return ret;
}

static int ds3911_write_power_on_reset(struct ds3911_dev *dac, u16 por,
                                       int channel) {
  int ret;
  dac->por[channel] = por;
  ret = ds3911_set_por(dac, channel);
  return ret;
}

static int ds3911_read_power_on_reset(struct ds3911_dev *dac, int channel) {
  return (int)dac->por[channel];
}

static int ds3911_read_polarity(struct ds3911_dev *dac, int channel) {
  return (int)dac->polarity[channel];
}

static ssize_t ds3911_write_softtxd(struct device *dev,
                                    struct device_attribute *attr,
                                    const char *buf, size_t len) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  bool softtxd;
  int ret;

  ret = strtobool(buf, &softtxd);
  if (ret) return ret;
  dac->SOFTTXD = softtxd;
  ret = ds3911_set_mode(dac);
  return ret ? ret : len;
}

static ssize_t ds3911_read_softtxd(struct device *dev,
                                   struct device_attribute *attr, char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  return sprintf(buf, "%d\n", dac->SOFTTXD);
}

static ssize_t ds3911_write_nsee(struct device *dev,
                                 struct device_attribute *attr, const char *buf,
                                 size_t len) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  bool nsee;
  int ret;

  ret = strtobool(buf, &nsee);
  if (ret) return ret;
  dac->nSEE = nsee;
  ret = ds3911_set_mode(dac);
  return ret ? ret : len;
}

static ssize_t ds3911_read_nsee(struct device *dev,
                                struct device_attribute *attr, char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  return sprintf(buf, "%d\n", dac->nSEE);
}

static ssize_t ds3911_write_max(struct device *dev,
                                struct device_attribute *attr, const char *buf,
                                size_t len, int channel) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  int ret;
  ret = kstrtou16(buf, 10, &dac->raw_max[channel]);
  if (ret) return ret;
  return len;
}

static ssize_t ds3911_read_max(struct device *dev,
                               struct device_attribute *attr, char *buf,
                               int channel) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  return sprintf(buf, "%d\n", dac->raw_max[channel]);
}

static ssize_t ds3911_write_max0(struct device *dev,
                                 struct device_attribute *attr, const char *buf,
                                 size_t len) {
  return ds3911_write_max(dev, attr, buf, len, 0);
}

static ssize_t ds3911_read_max0(struct device *dev,
                                struct device_attribute *attr, char *buf) {
  return ds3911_read_max(dev, attr, buf, 0);
}

static ssize_t ds3911_write_max1(struct device *dev,
                                 struct device_attribute *attr, const char *buf,
                                 size_t len) {
  return ds3911_write_max(dev, attr, buf, len, 1);
}

static ssize_t ds3911_read_max1(struct device *dev,
                                struct device_attribute *attr, char *buf) {
  return ds3911_read_max(dev, attr, buf, 1);
}

static ssize_t ds3911_write_max2(struct device *dev,
                                 struct device_attribute *attr, const char *buf,
                                 size_t len) {
  return ds3911_write_max(dev, attr, buf, len, 2);
}

static ssize_t ds3911_read_max2(struct device *dev,
                                struct device_attribute *attr, char *buf) {
  return ds3911_read_max(dev, attr, buf, 2);
}

static ssize_t ds3911_write_max3(struct device *dev,
                                 struct device_attribute *attr, const char *buf,
                                 size_t len) {
  return ds3911_write_max(dev, attr, buf, len, 3);
}

static ssize_t ds3911_read_max3(struct device *dev,
                                struct device_attribute *attr, char *buf) {
  return ds3911_read_max(dev, attr, buf, 3);
}

static ssize_t ds3911_write_aen(struct device *dev,
                                struct device_attribute *attr, const char *buf,
                                size_t len) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  bool aen;
  int ret;

  ret = strtobool(buf, &aen);
  if (ret) return ret;
  dac->AEN = aen;
  ret = ds3911_set_mode(dac);
  return ret ? ret : len;
}

static ssize_t ds3911_read_aen(struct device *dev,
                               struct device_attribute *attr, char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  return sprintf(buf, "%d\n", dac->AEN);
}

static ssize_t ds3911_write_tindex(struct device *dev,
                                   struct device_attribute *attr,
                                   const char *buf, size_t len) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  unsigned char tindex;
  int ret;

  ret = kstrtou8(buf, 0, &tindex);
  if (ret) return ret;
  return ds3911_set_tindex(dac, tindex);
}

static ssize_t ds3911_read_tindex(struct device *dev,
                                  struct device_attribute *attr, char *buf) {
  struct iio_dev *indio_dev = dev_to_iio_dev(dev);
  struct ds3911_dev *dac = iio_priv(indio_dev);
  return sprintf(buf, "%u\n", dac->tindex);
}

static int ds3911_set_value(struct ds3911_dev *dac, u16 val, int channel) {
  u16 write_word;
  u8 addr;
  if (val > dac->raw_max[channel]) {
    // We can not set this value because it is out of range.
    return -EINVAL;
  }
  dac->raw[channel] = val;
  write_word = (val & 0x3FF) << 6;
  addr = 0x10 + 2 * channel;
  return i2c_smbus_write_word_swapped(dac->client, addr, write_word);
}

static int ds3911_get_value(struct ds3911_dev *dac, int channel) {
  u8 addr;
  int ret;
  addr = 0x10 + 2 * channel;
  ret = i2c_smbus_read_word_swapped(dac->client, addr);
  if (ret) return ret;
  dac->raw[channel] = ((u16)ret) >> 6;
  return 0;
}

static int ds3911_write_raw(struct iio_dev *indio_dev,
                            struct iio_chan_spec const *chan, int val, int val2,
                            long mask) {
  int ret;
  struct ds3911_dev *dac = iio_priv(indio_dev);
  // TODO: LUT, DAC OFFSET
  switch (mask) {
    case IIO_CHAN_INFO_RAW:
      if (val < 0 || val > 1024) {
        ret = -EINVAL;
        break;
      }
      ret = ds3911_set_value(dac, val, chan->channel);
      break;
    case IIO_CHAN_INFO_ENABLE:
      // LUT enable.
      dac->lut_enable[chan->channel] = (bool)val;
      ret = ds3911_set_por(dac, chan->channel);
      break;
    case IIO_CHAN_INFO_CALIBBIAS:
      // Polarity
      ret = ds3911_write_polarity(dac, (bool)val, chan->channel);
      break;
    case IIO_CHAN_INFO_CALIBSCALE:
      // POR
      ret = ds3911_write_power_on_reset(dac, val, chan->channel);
      break;
    default:
      ret = -EINVAL;
      break;
  }
  return ret;
}

static int ds3911_read_raw(struct iio_dev *indio_dev,
                           struct iio_chan_spec const *chan, int *val,
                           int *val2, long m) {
  struct ds3911_dev *dac = iio_priv(indio_dev);
  int ret;
  switch (m) {
    case IIO_CHAN_INFO_SCALE:
      /* Corresponds to Vref / 2^(bits) */
      *val = dac->vref_mv;
      *val2 = 8;
      ret = IIO_VAL_FRACTIONAL_LOG2;
      break;
    case IIO_CHAN_INFO_RAW:
      *val = dac->raw[chan->channel];
      ret = IIO_VAL_INT;
      break;
    case IIO_CHAN_INFO_ENABLE:
      *val = (int)dac->lut_enable[chan->channel];
      ret = IIO_VAL_INT;
      break;
    case IIO_CHAN_INFO_CALIBBIAS:
      *val = ds3911_read_polarity(dac, chan->channel);
      ret = IIO_VAL_INT;
      break;
    case IIO_CHAN_INFO_CALIBSCALE:
      *val = ds3911_read_power_on_reset(dac, chan->channel);
      ret = IIO_VAL_INT;
      break;
    default:
      ret = -EINVAL;
      break;
  }
  return ret;
}

static IIO_DEVICE_ATTR(in_vcc, S_IRUGO, ds3911_show_vcc, NULL, 0);
static IIO_DEVICE_ATTR(in_temperature, S_IRUGO, ds3911_show_temperature, NULL,
                       0);
static IIO_DEVICE_ATTR(out_softtxd, S_IRUGO | S_IWUSR, ds3911_read_softtxd,
                       ds3911_write_softtxd, 0);
static IIO_DEVICE_ATTR(out_nsee, S_IRUGO | S_IWUSR, ds3911_read_nsee,
                       ds3911_write_nsee, 0);
static IIO_DEVICE_ATTR(out_aen, S_IRUGO | S_IWUSR, ds3911_read_aen,
                       ds3911_write_aen, 0);
static IIO_DEVICE_ATTR(out_tindex, S_IRUGO | S_IWUSR, ds3911_read_tindex,
                       ds3911_write_tindex, 0);
static IIO_DEVICE_ATTR(max0, S_IRUGO | S_IWUSR, ds3911_read_max0,
                       ds3911_write_max0, 0);
static IIO_DEVICE_ATTR(max1, S_IRUGO | S_IWUSR, ds3911_read_max1,
                       ds3911_write_max1, 0);
static IIO_DEVICE_ATTR(max2, S_IRUGO | S_IWUSR, ds3911_read_max2,
                       ds3911_write_max2, 0);
static IIO_DEVICE_ATTR(max3, S_IRUGO | S_IWUSR, ds3911_read_max3,
                       ds3911_write_max3, 0);

static struct attribute *ds3911_attributes[] = {
    &iio_dev_attr_in_vcc.dev_attr.attr,
    &iio_dev_attr_in_temperature.dev_attr.attr,
    &iio_dev_attr_out_softtxd.dev_attr.attr,
    &iio_dev_attr_out_nsee.dev_attr.attr,
    &iio_dev_attr_out_aen.dev_attr.attr,
    &iio_dev_attr_out_tindex.dev_attr.attr,
    &iio_dev_attr_max0.dev_attr.attr,
    &iio_dev_attr_max1.dev_attr.attr,
    &iio_dev_attr_max2.dev_attr.attr,
    &iio_dev_attr_max3.dev_attr.attr,
    NULL,
};

static const struct attribute_group ds3911_attribute_group = {
    .attrs = ds3911_attributes,
};

static const struct iio_info ds3911_info = {
    .read_raw = ds3911_read_raw,
    .write_raw = ds3911_write_raw,
    .attrs = &ds3911_attribute_group,
};

static int ds3911_probe(struct i2c_client *client,
                        const struct i2c_device_id *id) {
  struct device *dev = &client->dev;
  struct ds3911_dev *dac;
  struct iio_dev *iio;
  int ret;
  int n;

  iio = devm_iio_device_alloc(dev, sizeof(*dac));
  if (!iio) return -ENOMEM;

  dac = iio_priv(iio);
  i2c_set_clientdata(client, iio);
  dac->client = client;
  dac->regmap = devm_regmap_init_i2c(client, &ds3911_regmap_config);

  // We create all the binary files for reading and write the LUT and offset
  // values
  dac->lut[0].attr.name = "lut0";
  dac->lut[0].attr.mode = S_IWUSR | S_IRUGO;
  dac->lut[0].size = DS3911_LUT_SIZE;
  dac->lut[0].private = dac;
  dac->lut[0].read = ds3911_read_LUT_ch0;
  dac->lut[0].write = ds3911_write_LUT_ch0;

  dac->lut[1].attr.name = "lut1";
  dac->lut[1].attr.mode = S_IWUSR | S_IRUGO;
  dac->lut[1].size = DS3911_LUT_SIZE;
  dac->lut[1].private = dac;
  dac->lut[1].read = ds3911_read_LUT_ch1;
  dac->lut[1].write = ds3911_write_LUT_ch1;

  dac->lut[2].attr.name = "lut2";
  dac->lut[2].attr.mode = S_IWUSR | S_IRUGO;
  dac->lut[2].size = DS3911_LUT_SIZE;
  dac->lut[2].private = dac;
  dac->lut[2].read = ds3911_read_LUT_ch2;
  dac->lut[2].write = ds3911_write_LUT_ch2;

  dac->lut[3].attr.name = "lut3";
  dac->lut[3].attr.mode = S_IWUSR | S_IRUGO;
  dac->lut[3].size = DS3911_LUT_SIZE;
  dac->lut[3].private = dac;
  dac->lut[3].read = ds3911_read_LUT_ch3;
  dac->lut[3].write = ds3911_write_LUT_ch3;

  dac->offset[0].attr.name = "offset0";
  dac->offset[0].attr.mode = S_IWUSR | S_IRUGO;
  dac->offset[0].size = DS3911_OFFSET_SIZE;
  dac->offset[0].private = dac;
  dac->offset[0].read = ds3911_read_OFFSET_ch0;
  dac->offset[0].write = ds3911_write_OFFSET_ch0;

  dac->offset[1].attr.name = "offset1";
  dac->offset[1].attr.mode = S_IWUSR | S_IRUGO;
  dac->offset[1].size = DS3911_OFFSET_SIZE;
  dac->offset[1].private = dac;
  dac->offset[1].read = ds3911_read_OFFSET_ch1;
  dac->offset[1].write = ds3911_write_OFFSET_ch1;

  dac->offset[2].attr.name = "offset2";
  dac->offset[2].attr.mode = S_IWUSR | S_IRUGO;
  dac->offset[2].size = DS3911_OFFSET_SIZE;
  dac->offset[2].private = dac;
  dac->offset[2].read = ds3911_read_OFFSET_ch2;
  dac->offset[2].write = ds3911_write_OFFSET_ch2;

  dac->offset[3].attr.name = "offset3";
  dac->offset[3].attr.mode = S_IWUSR | S_IRUGO;
  dac->offset[3].size = DS3911_OFFSET_SIZE;
  dac->offset[3].private = dac;
  dac->offset[3].read = ds3911_read_OFFSET_ch3;
  dac->offset[3].write = ds3911_write_OFFSET_ch3;

  for (n = 0; n < 4; n++) {
    ret = device_create_bin_file(&client->dev, &dac->lut[n]);
    ret = device_create_bin_file(&client->dev, &dac->offset[n]);
  }

  iio->dev.parent = &client->dev;
  iio->info = &ds3911_info;
  iio->name = DS3911_DRV_NAME;
  iio->channels = ds3911_channels;
  iio->num_channels = ARRAY_SIZE(ds3911_channels);
  iio->modes = INDIO_DIRECT_MODE;
  ret = iio_device_register(iio);
  if (ret < 0) return ret;

  //
  dac->vref_mv = 5000; /* mv */
  dac->raw_max[0] = 0;
  dac->raw_max[1] = 0;
  dac->raw_max[2] = 0;
  dac->raw_max[3] = 0;

  //
  ds3911_get_tindex(dac);
  ds3911_get_mode(dac);
  for (n = 0; n < ARRAY_SIZE(ds3911_channels); n++) {
    ds3911_get_por(dac, n);
    ds3911_get_value(dac, n);
  }
  return 0;
}

static int ds3911_remove(struct i2c_client *client) {
  struct iio_dev *iio = i2c_get_clientdata(client);
  iio_device_unregister(iio);
  return 0;
}

static const struct i2c_device_id ds3911_id[] = {{
                                                     "maxim,ds3911",
                                                 },
                                                 {}};
MODULE_DEVICE_TABLE(i2c, ds3911_id);

static struct of_device_id ds3911_of_match[] = {
    {
        .compatible = "maxim,ds3911",
    },
    {},
};
MODULE_DEVICE_TABLE(of, ds3911_of_match);

static struct i2c_driver ds3911_driver = {
    .driver =
        {
            .name = DS3911_DRV_NAME,
            .owner = THIS_MODULE,
            .of_match_table = of_match_ptr(ds3911_of_match),
        },
    .probe = ds3911_probe,
    .remove = ds3911_remove,
    .id_table = ds3911_id,
};
module_i2c_driver(ds3911_driver);

MODULE_AUTHOR("Refael Whyte <r.whyte@chronoptics.com>");
MODULE_DESCRIPTION("DS3911 10-bit DAC");
MODULE_LICENSE("GPL");
