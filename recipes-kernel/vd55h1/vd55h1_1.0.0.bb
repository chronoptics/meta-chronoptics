DESCRIPTION = "DS3911 Module driver" 
HOMEPAGE = "www.chronoptics.com"

LICENSE = "GPL-2.0-only" 
LIC_FILES_CHKSUM = "file://LICENSE;md5=801f80980d171dd6425610833a22dbe6"

inherit module

SRCREV = "${AUTOREV}"
SRC_URI = "git://git@bitbucket.org/chronoptics/vd55h1_driver.git;protocol=https;branch=rev_d1_silicon"

S = "${WORKDIR}/git"

RPROVIDES:${PN} += "kernel-module-vd55h1"
