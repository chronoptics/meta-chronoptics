FILESEXTRAPATHS:append := "${THISDIR}/files:"

DTS_SRC = "devicetrees"
DTS_DEST = "${STAGING_KERNEL_DIR}/arch/arm64/boot/dts/freescale"
LINUX_VERSION_EXTENSION = "-variscite"
INSANE_SKIP:${PN} += "version-going-backwards"

SRC_URI:append = " \
    file://${DTS_SRC}/kea-c-common.dtsi \  
    file://${DTS_SRC}/kea-st.dts \
    file://defconfig \
"
KBUILD_DEFCONFIG:imx8mq-var-dart = ""

# Custom task to prepare device trees
do_prepare_kea_dts() {
    mkdir -p ${DTS_DEST}

    cp "${WORKDIR}/${DTS_SRC}/kea-c-common.dtsi" "${DTS_DEST}"
    cp "${WORKDIR}/${DTS_SRC}/kea-st.dts" "${DTS_DEST}"
}
addtask prepare_kea_dts after do_unpack before do_patch

# KeaC related items. 
SRC_URI:append = "\
    file://0001-MIPI-CSI2-Add-configuration-options.patch \
    file://0002-Increase-max-video-memory-from-64-to-512MB.patch \
    file://0003-Enable-two_8bit_sensor_mode-based-on-video-format.patch \ 
    file://0004-Add-RAW12-support.patch \
    file://0005-Fix-problems-with-non-interlaced-images.patch \
    file://0006-Increase-max-buffer-size.patch \
    file://0007-Change-ina219-driver-settings.patch \
    file://0008-Remove-csi_tvdec_enable.patch \
    file://0009-Print-base-address-switching-error.patch \
    file://0010-Propagate-failure-to-start-stream.patch \
    file://0011-DT-Remove-all-items-related-to-wm8904.patch \
"
