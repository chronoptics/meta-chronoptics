DESCRIPTION = "MLX75027 Module driver" 
HOMEPAGE = "www.chronoptics.com"

LICENSE = "GPL-2.0-only" 
LIC_FILES_CHKSUM = "file://LICENSE;md5=801f80980d171dd6425610833a22dbe6"

inherit module

SRC_URI = "file://Makefile \
           file://mlx75027.c \
           file://mlx75027.h \
           file://LICENSE"

S = "${WORKDIR}"

RPROVIDES:${PN} += "kernel-module-mlx75027"