DESCRIPTION = "Simple viewer to demonstrate displaying depth images on the GUI"
LICENSE  = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d8927f3331d2b3e321b7dd1925166d25"

inherit cmake pkgconfig

DEPENDS = "opencv tof openmp" 
RDEPENDS:${PN} += "libgomp libopencv-core libopencv-imgproc libopencv-highgui"

COMPATIBLE_MACHINE = "kea-c"

SRCREV = "${AUTOREV}"
SRC_URI = "git://github.com/rzw2/kea-opencv-viewer.git;branch=main;protocol=https"

S = "${WORKDIR}/git"

EXTRA_OECMAKE = ""
