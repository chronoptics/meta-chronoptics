SUMMARY = "Add generic configuration and calibration files to the rootfs"
HOMEPAGE = "www.chronoptics.com"
LICENSE = "CLOSED"

# The configuration directory to install all the files
base_configdir = "/home/root/config"

# The files provided
SRC_URI = " \
    file://calibration.ccf \
    "

COMPATIBLE_MACHINE = "kea-c"

# Copy over files and change permissions
do_install() {
        install -d ${D}${base_configdir}

        mkdir -p ${D}${base_configdir}
        cp ${WORKDIR}/calibration.ccf ${D}${base_configdir}/calibration.ccf
}

FILES:${PN} += "${base_configdir}/calibration.ccf"
