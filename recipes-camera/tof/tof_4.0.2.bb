DESCRIPTION = "Chronoptics Time of Flight library" 
HOMEPAGE = "www.chronoptics.com"

LICENSE = "CLOSED" 
LIC_FILES_CHKSUM = "file://tof/share/tof/LICENSE;md5=ac6037c56c8065dbc45c60bb4a59e52f"

inherit systemd features_check

SRC_URI = " \
    https://docs.chronoptics.com/releases/v4.0.2/tof-linux-aarch64.tar.gz;subdir=tof;md5sum=d3c154d8281a87551284a171ac5c5060 \
    file://monitord.service \
    file://interfaces.service \
"

S = "${WORKDIR}"

COMPATIBLE_MACHINE = "kea-c"

RDEPENDS:${PN}-interfaces += "networkmanager glib-2.0 libaio libubootenv libusbgx libimxvpuapi2 libimxdmabuffer"
RDEPENDS:${PN} += "libopencl-imx libimxvpuapi2 libubootenv libimxdmabuffer networkmanager glib-2.0"
REQUIRED_DISTRO_FEATURES= "systemd"

do_install () {
    cd ${S}

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 monitord.service ${D}${systemd_system_unitdir}
    install -m 0644 interfaces.service ${D}${systemd_system_unitdir}

    cd tof

    install -d ${D}/usr/ 
    # Copied from bin_package.bbclass
    tar --no-same-owner -cpf - . | tar --no-same-owner -xpf - -C ${D}/usr/
}

PACKAGES =+ "${PN}-interfaces"

FILES:${PN}-interfaces = "/usr/bin/interfaces ${systemd_system_unitdir}/interfaces.service"
FILES:${PN}-doc = "/usr/share/tof/"
FILES:${PN} += "/"

SYSTEMD_PACKAGES += "${PN}-interfaces"

SYSTEMD_SERVICE:${PN} = "monitord.service"
SYSTEMD_SERVICE:${PN}-interfaces = "interfaces.service"

INSANE_SKIP:${PN} += "already-stripped"
