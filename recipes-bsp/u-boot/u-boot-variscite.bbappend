FILESEXTRAPATHS:append := "${THISDIR}/files:"

SRC_URI:append = " \
    file://0001-Set-default-device-tree.patch \
    file://0002-Add-rootrw-argument.patch \
    file://0003-Remove-splash.patch \
    file://0004-Enable-boot-count.patch \
    file://0005-Replace-netboot-with-altbootcmd.patch \
"
