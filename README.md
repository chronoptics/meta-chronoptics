# Chronoptics yocto layer

Usage instructions

- Clone meta-chronoptics into sources
- Add layer to bblayers.conf
- Update MACHINE in local.conf to kea-c
- Add tof library to the build image

The following packages are available:

- tof: Contains the library and python wrappers
- tof-dev: Contains c and c++ header files
- tof-doc: Contains documentation and examples
- tof-gige-interface: Contains the executable that is responsible for gige communication
- tof-usb-device: Contains the executable that is responsible for usb device communication


## Detailed Install Instructions 

The Kea camera is built around the DART-MX8M system on module from Variscite and a MLX75027 ToF image sensor from Melexis. 

We recommend building on 
 - Ubuntu 20.04 lts 
We recommend using the kirkstone release or later
 - Kirkstone, https://variwiki.com/index.php?title=DART-MX8M_Yocto&release=mx8m-yocto-kirkstone-5.15-2.0.x-v1.0

The documentation for building Yocto for the Variscite DART-MX8M is here https://variwiki.com/index.php?title=DART-MX8M
More information about the MLX75027 image sensor is here https://www.melexis.com/en/product/MLX75027/Automotive-VGA-Time-Of-Flight-Sensor
Other documentation about the Chronoptics Kea camera is here 

# Building Camera SD Card
These are modified from (https://variwiki.com/index.php?title=Yocto_Build_Release&release=mx8m-yocto-kirkstone-5.15-2.0.x-v1.0) 

## 1. Installing required packages

Please make sure your host PC is running Ubuntu 20.04 64-bit and install the following packages:

    sudo apt-get install gawk wget git diffstat unzip texinfo gcc-multilib build-essential chrpath socat cpio python python3 python3-pip python3-pexpect  xz-utils debianutils iputils-ping libsdl1.2-dev xterm

    sudo apt-get install autoconf libtool libglib2.0-dev libarchive-dev python3-git sed cvs subversion coreutils texi2html docbook-utils python-pysqlite2 help2man make gcc g++ desktop-file-utils libgl1-mesa-dev libglu1-mesa-dev mercurial automake groff curl lzop asciidoc u-boot-tools dos2unix mtd-utils pv libncurses5 libncurses5-dev libncursesw5-dev libelf-dev zlib1g-dev bc rename

## 2. Download Yocto Kirkstone based on Freescale Community BSP

Configure git (if you have not done so already) 

    git config --global user.name "Your Name"
    git config --global user.email "Your Email"

    mkdir ~/bin 
    curl https://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo
    export PATH=~/bin:$PATH

    mkdir ~/var-fslc-yocto
    cd ~/var-fslc-yocto

Download specific revision

    repo init -u https://github.com/varigit/variscite-bsp-platform.git -b refs/tags/mx8m-yocto-kirkstone-5.15-2.0.x-v1.0 -m kirkstone-5.15.xml
    repo sync -j4

Now Add the Chronoptics Kea meta layer to the sources directory 

    cd ~/var-fslc-yocto/sources
    git clone https://bitbucket.org/chronoptics/meta-chronoptics.git

Init the build directory 

    cd ~/var-fslc-yocto
    MACHINE=kea-c DISTRO=fslc-xwayland . setup-environment build_xwayland

There are sometimes problems with the connectivity detection of yocto, so change this file 

    ~/var-fslc-yocto/build_xwayland/conf/local.conf 

and add the following line at the bottom:

    CONNECTIVITY_CHECK_URIS = "https://www.google.com/" 

The creation of a read-only rootfs is supported, you only need to add the following to the `build_xwayland/conf/local.conf` file:

    INITRAMFS_IMAGE = "kea-image-initramfs"
    INITRAMFS_IMAGE_BUNDLE = "1"

This will create an initramfs which will mount the third partition with overlayfs over the rootfs.  

Add the Chronoptics layer to the bblayers file, in  

    ~/var-fslc-yocto/build_xwayland/conf/bblayers.conf 

Add 

    ${BSPDIR}/sources/meta-chronoptics 

To the BBLAYERS variable 

Now build the most basic kea image

    cd ~/var-fslc-yocto/build_xwayland
    bitbake kea-image-base 

This is the bare minimum image to run the Chronoptics Kea Camera. 

## 3. Remote update image

To enable remote update image generation make sure that the `${BSPDIR}/sources/meta-swupdate` layer is added/uncommented in the `conf/bblayers.conf` file.

Then to generate an update image run

    bitbake kea-image-swu

This will create an update image in `build_xwayland/tmp/deploy/images/kea-c/kea-image-swu-kea-c.swu`. On cameras released since October 2022 this image can be uploaded directly to the camera by going to a website in your preferred browser:

    http://<camera_ip>:8080

For example

    http://192.168.1.206:8080

There you can upload the created image and after a successful update the camera will be running the software built on your system.

## 4. Build SD Card Image 

Copy required build artifacts to the folder you will be running create_sd.sh from

    cp ~/var-fslc-yocto/build_xwayland/tmp/deploy/images/kea-c/imx-boot-kea-c-sd.bin-flash_evk ~/boot.bin
    cp ~/var-fslc-yocto/build_xwayland/tmp/deploy/images/kea-c/kea-image-base-kea-c.tar.gz ~/rootfs.tar.gz

Run create_sd.sh script 

    sudo ~/var-fslc-yocto/meta-chronoptics/scripts/create_sd.sh /dev/sdX

Where X is the SD drive, for example /dev/sda

## 5. Selecting Device Tree Blob (DTB) 
There are two Kea camera hardware versions 
 - USB Device (Default), the RGB camera module is connected as a USB Device. 
 - USB Host (No RGB), can plug USB devices into Host port. 

Mount the rootfs partition of the SD card. In the rootfs there are several DTB options

    kea-c-rev-b-device-wifi.dtb 
    kea-c-rev-b-device.dtb 
    kea-c-rev-b-host.dtb

By default the DTB links to the kea-c-rev-b-device.dtb, to switch to the host image update the fdt_file variable in u-boot to "kea-c-rev-b-host.dtb". This can be done after boot:

    fw_setenv fdt_file kea-c-rev-b-host.dtb

To enable wifi set the `kea-c-rev-b-device-wifi.dtb` as the device tree blob to use. Also make sure the variscite-wifi.service is enabled, since this will automatically disable itself when the camera is booted using a different device tree blob.

## 6. Add calibration file
The SD image does not contain a calibration file, you will need to download the original calibration file from the camera, and replace the calibration file (calibration.cff) in the the kea-calibration recipe. Then add the kea-calibration recipe to the conf\local.conf to the IMAGE_INSTALL:append variable. Or copy your calibration file to /home/root/config/calibration.ccf on the SD card. 

## 7. Boot Camera 
The SD card holder is located in the top of the camera. It is easiest to remove the SD card with tweezers. 

## 8. Example HDMI Output 

To create an image with HDMI output:

    cd ~/var-fslc-yocto/build_xwayland
    bitbake kea-gui-example

You need to add the calibration file to the image as well. 

Create the SD Card. 

Plug in the SD card and a HDMI screen. You can view the camera output to the screen by running 

    kea_opencv_viewer 

From the terminal, the cameras depth and intensity images will be displayed. The source code for kea-opencv-viewer is on github which demonstrates how to use the EmbeddedKeaCamera class.  

## 9. Example Google Coral 
See the instructions at https://github.com/rzw2/meta-chronoptics-coral
