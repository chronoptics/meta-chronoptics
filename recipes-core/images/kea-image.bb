DESCRIPTION = "Default image for kea time-of-flight depth camera from Chronoptics"
LICENSE = "CLOSED"

require kea-image-base.bb

IMAGE_FEATURES += " debug-tweaks"

CORE_IMAGE_EXTRA_INSTALL += "   packagegroup-core-full-cmdline \
                                packagegroup-core-ssh-dropbear \
" 
