DESCRIPTION = "Development image for kea time-of-flight depth camera from Chronoptics"
LICENSE = "CLOSED"

require kea-image.bb

CORE_IMAGE_EXTRA_INSTALL += "	gdb \
                                gdbserver \
                                perl-module-getopt-long \
                                perl-module-file-basename \
                                perl-module-file-glob \
                                valgrind \
" 
