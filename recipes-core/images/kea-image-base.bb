DESCRIPTION = "Base image for kea time-of-flight depth camera from Chronoptics"
LICENSE = "CLOSED"

inherit core-image

CORE_IMAGE_EXTRA_INSTALL += "	tof \
								tof-interfaces \
" 

DEPENDS += "kea-image-initramfs imx-boot"

ROOTFS_POSTPROCESS_COMMAND:append = "install_initramfs;"
install_initramfs() {
	file_name=${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}-${INITRAMFS_LINK_NAME}.bin
	if [ -f ${file_name} ] ; then
    	cp ${file_name} ${IMAGE_ROOTFS}/boot/${KERNEL_IMAGETYPE}
	fi
}
