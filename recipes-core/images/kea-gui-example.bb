DESCRIPTION = "Image demonstrating GUI capabilities of camera"
LICENSE = "CLOSED"

require recipes-fsl/images/fsl-image-gui.bb

CORE_IMAGE_EXTRA_INSTALL += "	tof \
								tof-gige-interface \
								tof-usb-device \
                                kea-opencv-viewer \
" 