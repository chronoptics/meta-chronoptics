#!/bin/sh

# Enable strict shell mode
set -euo pipefail

PATH=/sbin:/bin:/usr/sbin:/usr/bin

MOUNT="/bin/mount"
UMOUNT="/bin/umount"

INIT="/sbin/init"
ROOT_ROINIT="/sbin/init"

ROOT_MOUNT="/mnt"
ROOT_RODEVICE=""
ROOT_RWDEVICE=""
ROOT_ROMOUNT="/media/rfs/ro"
ROOT_RWMOUNT="/media/rfs/rw"

ROOT_ROMOUNTOPTIONS_DEVICE="noatime,nodiratime"
ROOT_RWMOUNTOPTIONS_DEVICE="rw,noatime"

early_setup() {
	mkdir -p /proc
	mkdir -p /sys
	$MOUNT -t proc proc /proc
	$MOUNT -t sysfs sysfs /sys
	grep -w "/dev" /proc/mounts >/dev/null || $MOUNT -t devtmpfs none /dev
}

read_args() {
	[ -z "${CMDLINE+x}" ] && CMDLINE=`cat /proc/cmdline`
	for arg in $CMDLINE; do
		# Set optarg to option parameter, and '' if no parameter was
		# given
		optarg=`expr "x$arg" : 'x[^=]*=\(.*\)' || echo ''`
		case $arg in
			root=*)
				ROOT_RODEVICE=$optarg ;;
			rootinit=*)
				ROOT_ROINIT=$optarg ;;
			rootoptions=*)
				ROOT_ROMOUNTOPTIONS_DEVICE="$optarg" ;;
			rootrw=*)
				ROOT_RWDEVICE=$optarg ;;
			rootrwoptions=*)
				ROOT_RWMOUNTOPTIONS_DEVICE="$optarg" ;;
			init=*)
			INIT=$optarg ;;
		esac
	done
}

fatal() {
	echo "rorootfs-overlay: $1" >$CONSOLE
	echo >$CONSOLE
	exec sh
}

log() {
	echo "rorootfs-overlay: $1" >$CONSOLE
}

early_setup

[ -z "${CONSOLE+x}" ] && CONSOLE="/dev/console"

read_args

check_available() {
	log "Check if $1 is available"
	counter=0

	while [ ! -b $1 ]; do
		sleep 0.1
		counter=$((counter + 1))
		if [ $counter -ge 50 ]; then
			fatal "$1 was not available"
		fi
	done
}

mount_and_boot() {
	mkdir -p $ROOT_MOUNT $ROOT_ROMOUNT $ROOT_RWMOUNT

	check_available $ROOT_RODEVICE 
	check_available $ROOT_RWDEVICE

	# Mount root file system to new mount-point
	if ! $MOUNT -o $ROOT_ROMOUNTOPTIONS_DEVICE $ROOT_RODEVICE $ROOT_ROMOUNT; then
		fatal "Could not mount read-only rootfs"
	fi

	# Remounting root file system as read only.
	if ! $MOUNT -o remount,ro $ROOT_ROMOUNT; then
		fatal "Could not remount read-only rootfs as read only"
	fi

	# If future init is the same as current file, use $ROOT_ROINIT
	# Tries to avoid loop to infinity if init is set to current file via
	# kernel command line
	if cmp -s "$0" "$INIT"; then
		INIT="$ROOT_ROINIT"
	fi

	# Mount read-write file system into initram root file system
	if ! $MOUNT -o $ROOT_RWMOUNTOPTIONS_DEVICE $ROOT_RWDEVICE $ROOT_RWMOUNT; then
		fatal "Could not mount read-write rootfs"
	fi

	# Create/Mount overlay root file system
	mkdir -p $ROOT_RWMOUNT/upperdir $ROOT_RWMOUNT/work
	if ! $MOUNT -t overlay overlay \
		-o "$(printf "%s%s%s" \
			"lowerdir=$ROOT_ROMOUNT," \
			"upperdir=$ROOT_RWMOUNT/upperdir," \
			"workdir=$ROOT_RWMOUNT/work")" \
		$ROOT_MOUNT ; then
		fatal "Could not mount overlayfs"
	fi

	# Move read-only and read-write root file system into the overlay
	# file system
	mkdir -p $ROOT_MOUNT/$ROOT_ROMOUNT $ROOT_MOUNT/$ROOT_RWMOUNT
	$MOUNT -n --move $ROOT_ROMOUNT ${ROOT_MOUNT}/$ROOT_ROMOUNT
	$MOUNT -n --move $ROOT_RWMOUNT ${ROOT_MOUNT}/$ROOT_RWMOUNT

	$MOUNT -n --move /proc ${ROOT_MOUNT}/proc
	$MOUNT -n --move /sys ${ROOT_MOUNT}/sys
	$MOUNT -n --move /dev ${ROOT_MOUNT}/dev

	cd $ROOT_MOUNT

	# switch to actual init in the overlay root file system
	exec chroot $ROOT_MOUNT $INIT ||
		fatal "Couldn't chroot, dropping to shell"
}

mount_and_boot
