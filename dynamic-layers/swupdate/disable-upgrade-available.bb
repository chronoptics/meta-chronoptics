DESCRIPTION = "Disable upgrade availabe" 
LICENSE = "CLOSED" 

inherit systemd

SRC_URI = "\
    file://disable-upgrade-available.service \
    file://disable-upgrade-available.sh \
"

S = "${WORKDIR}"

do_install() {
    cd ${S}

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 disable-upgrade-available.service ${D}${systemd_system_unitdir}
    install -m 0755 disable-upgrade-available.sh ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE:${PN} = "disable-upgrade-available.service"

FILES:${PN} = "${systemd_system_unitdir}/"