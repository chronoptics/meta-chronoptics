DESCRIPTION = "Generate software update package"
LICENSE = "CLOSED"

SRC_URI = "\
    file://sw-description \
    file://update.sh \
"

inherit swupdate

IMAGE_DEPENDS = "kea-image u-boot-variscite"
SWUPDATE_IMAGES = "kea-image imx-boot-kea-c-sd.bin-flash_evk"
SWUPDATE_IMAGES_FSTYPES[kea-image] = ".tar.gz"
