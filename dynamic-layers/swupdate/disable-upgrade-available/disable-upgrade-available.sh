#!/bin/sh

# Always check if the upgrade_available var is set
# to reduce write cycles to the uenv.
ISUPGRADING=$(fw_printenv upgrade_available | awk -F'=' '{print $2}')
echo "upgrade_available=$ISUPGRADING"

if [ -z "$ISUPGRADING" ]
then
    echo "No RootFs update pending"
else
    echo "RootFs update pending, verifying system"

    # For now, having the system succesfully boot is verification enough
    # Perform extra checks here.

    fw_setenv upgrade_available 0
    fw_setenv bootcount 0
fi