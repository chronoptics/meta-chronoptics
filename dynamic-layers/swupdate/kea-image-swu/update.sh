#!/bin/sh

if [ $# -lt 1 ]; then
	exit 0;
fi

function get_current_root_device
{
	for i in `cat /proc/cmdline`; do
		if [ ${i:0:5} = "root=" ]; then
			CURRENT_ROOT="${i:5}"
		fi
	done
}

function get_update_part
{
	CURRENT_PART="${CURRENT_ROOT: -1}"
	if [ $CURRENT_PART = "1" ]; then
		UPDATE_PART="2";
	else
		UPDATE_PART="1";
	fi
}

function get_update_device
{
	UPDATE_ROOT=${CURRENT_ROOT%?}${UPDATE_PART}
}

function format_update_device
{
	umount $UPDATE_ROOT
	mkfs.ext4 $UPDATE_ROOT -F -L rootfs${UPDATE_PART} -q
}

function set_uboot_env
{
	fw_setenv mmcpart $UPDATE_PART
	fw_setenv upgrade_available 1
	fw_setenv bootcount 0
}

if [ $1 == "preinst" ]; then
	# get the current root device
	get_current_root_device

	# get the device to be updated
	get_update_part
	get_update_device

	# format the device to be updated
	format_update_device

	# create a symlink for the update process
	ln -sf $UPDATE_ROOT /dev/update
fi

if [ $1 == "postinst" ]; then
	get_current_root_device

	get_update_part

	CURRENT_BLK_DEV=${CURRENT_ROOT%p?}

	# Update boot loader
	dd if=/boot.bin of=${CURRENT_BLK_DEV} bs=1K seek=33; sync	
	rm boot.bin

	# Adjust u-boot-fw-utils for eMMC on the installed rootfs
	mount -t ext4 /dev/update /tmp/datadst
	sed -i "s/\/dev\/mmcblk./${CURRENT_BLK_DEV//\//\\/}/" /tmp/datadst/etc/fw_env.config

	chroot /tmp/datadst
	set_uboot_env

	umount /dev/update
fi
