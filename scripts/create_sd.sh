#!/bin/bash
set -e

# Sizes are in MiB
readonly BOOTLOAD_RESERVE_SIZE=8
readonly ROOTFS_SIZE=2048
readonly SPARE_SIZE=4
readonly LOOP_MAJOR=7

readonly ROOTFS_FILE="rootfs.tar.gz"
readonly UBOOT_FILE="boot.bin"

# This function performs sanity check to verify  that the target device is removable devise of proper size
function check_device()
{
	# Check that parameter is a valid block device
	if [ ! -b "$1" ]; then
          echo "$1 is not a valid block device, exiting"
	  exit 1
        fi

	local dev=$(basename $1)

	# Check that /sys/block/$dev exists
	if [ ! -d /sys/block/$dev ]; then
	  echo "Directory /sys/block/${dev} missing, exiting"
	  exit 1
        fi

	# Get device parameters
	local removable=$(cat /sys/block/${dev}/removable)
	local size_bytes=$((512*$(cat /sys/class/block/${dev}/size)))
	local size_gb=$((size_bytes/1000000000))

	# Non-removable SD card readers require additional check
	if [ "${removable}" != "1" ]; then
          local drive=$(udisksctl info -b /dev/${dev}|grep "Drive:"|cut -d"'" -f 2)
          local mediaremovable=$(gdbus call --system --dest org.freedesktop.UDisks2 \
				  --object-path ${drive} --method org.freedesktop.DBus.Properties.Get \
				  org.freedesktop.UDisks2.Drive MediaRemovable)
	  if [[ "${mediaremovable}" = *"true"* ]]; then
	    removable=1
	  fi
	fi

	# Check that device is either removable or loop
	if [ "$removable" != "1" -a $(stat -c '%t' /dev/$dev) != ${LOOP_MAJOR} ]; then
          echo "$1 is not a removable device, exiting"
	  exit 1
        fi

	# Check that device is attached
	if [ ${size_bytes} -eq 0 ]; then
          echo "$1 is not attached, exiting"
          exit 1
	fi

	# Check that device has a valid size
	echo "Detected removable device $1, size=${size_gb}GB"
}

echo "========================================="
echo "=  Chronoptics sd card creation script  ="
echo "========================================="

help() {
	bn=`basename $0`
	echo " Usage: $bn <options> device_node"
	echo
	echo " options:"
	echo " -h		display this Help message"
	echo " -s		only Show partition sizes to be written, without actually write them"
	echo
}

if [[ $EUID -ne 0 ]] ; then
	echo "This script must be run with super-user privileges"
	exit 1
fi

TEMP_DIR=./var_tmp
P2_MOUNT_DIR=${TEMP_DIR}/rootfs

# Parse command line
moreoptions=1
node="na"
cal_only=0

while [ "$moreoptions" = 1 -a $# -gt 0 ]; do
	case $1 in
	    -h) help; exit 3 ;;
	    -s) cal_only=1 ;;
	    *)  moreoptions=0; node=$1 ;;
	esac
	[ "$moreoptions" = 0 ] && [ $# -gt 1 ] && help && exit 1
	[ "$moreoptions" = 1 ] && shift
done
part=""
if [[ $node == *mmcblk* ]] || [[ $node == *loop* ]] ; then
	part="p"
fi

# allow only removable/loopback devices, to protect host PC
check_device $node

# Call sfdisk to get total card size
TOTAL_SIZE=`sfdisk -s ${node}`
TOTAL_SIZE=`expr ${TOTAL_SIZE} / 1024`

RWFS_SIZE=`expr ${TOTAL_SIZE} - ${BOOTLOAD_RESERVE_SIZE} - ${ROOTFS_SIZE} - ${ROOTFS_SIZE} - ${SPARE_SIZE}`

if [ "${cal_only}" -eq "1" ]; then
cat << EOF
BOOTLOADER (No Partition) : ${BOOTLOAD_RESERVE_SIZE}MiB
ROOT-FS-1                 : ${ROOTFS_SIZE}MiB
ROOT-FS-2                 : ${ROOTFS_SIZE}MiB
RW-FS                     : ${RWFS_SIZE}MiB
EOF
exit 3
fi


function delete_device
{
	echo
	echo "Deleting current partitions"
	for ((i=0; i<=10; i++))
	do
		if [[ -e ${node}${part}${i} ]] ; then
			dd if=/dev/zero of=${node}${part}${i} bs=512 count=1024 2> /dev/null || true
		fi
	done
	sync

	((echo d; echo 1; echo d; echo 2; echo d; echo 3; echo d; echo w) | fdisk $node &> /dev/null) || true
	sync

	dd if=/dev/zero of=$node bs=1M count=${BOOTLOAD_RESERVE_SIZE}
	sync; sleep 1
}

function ceildiv
{
    local num=$1
    local div=$2
    echo $(( (num + div - 1) / div ))
}

function create_parts
{
	echo
	echo "Creating new partitions"
	BLOCK=`echo ${node} | cut -d "/" -f 3`
	SECT_SIZE_BYTES=`cat /sys/block/${BLOCK}/queue/physical_block_size`

	BOOTLOAD_RESERVE_SIZE_BYTES=$((BOOTLOAD_RESERVE_SIZE * 1024 * 1024))
	ROOTFS_SIZE_BYTES=$((ROOTFS_SIZE * 1024 * 1024))
	RWFS_SIZE_BYTES=$((RWFS_SIZE * 1024 * 1024))

	PART1_START=`ceildiv ${BOOTLOAD_RESERVE_SIZE_BYTES} ${SECT_SIZE_BYTES}`
	PART1_SIZE=`ceildiv ${ROOTFS_SIZE_BYTES} ${SECT_SIZE_BYTES}`

	PART2_START=`expr ${PART1_START} + ${PART1_SIZE}`
	PART2_SIZE=`ceildiv ${ROOTFS_SIZE_BYTES} ${SECT_SIZE_BYTES}`

	PART3_START=`expr ${PART2_START} + ${PART2_SIZE}`
	PART3_SIZE=`ceildiv ${RWFS_SIZE_BYTES} ${SECT_SIZE_BYTES}`

sfdisk --force -uS ${node} &> /dev/null << EOF
${PART1_START},${PART1_SIZE},83
${PART2_START},${PART2_SIZE},83
${PART3_START},${PART3_SIZE},83
EOF

	sync; sleep 1
	fdisk -l $node
}

function format_parts
{
	echo
	echo "Formatting partitions"
	mkfs.ext4 ${node}${part}1 -L rootfs1
	mkfs.ext4 ${node}${part}2 -L rootfs2
	mkfs.f2fs ${node}${part}3 -l data
	sync; sleep 1
}

function install_bootloader
{
	echo
	echo "Installing U-Boot"
	dd if=${UBOOT_FILE} of=${node} bs=1K seek=33; sync
}

function mount_parts
{
	mkdir -p ${P2_MOUNT_DIR}
	sync
	mount ${node}${part}1  ${P2_MOUNT_DIR}
}

function unmount_parts
{
	umount ${P2_MOUNT_DIR}
	rm -rf ${TEMP_DIR}
}

function install_yocto
{
	echo
	echo "Installing Yocto Root File System"
	pv ${ROOTFS_FILE} | tar -xz -C ${P2_MOUNT_DIR}/
}

umount ${node}${part}*  2> /dev/null || true

delete_device
create_parts
format_parts

mount_parts
install_yocto

echo
echo "Syncing"
sync | pv -t

unmount_parts

install_bootloader

echo
echo "Done"

exit 0
