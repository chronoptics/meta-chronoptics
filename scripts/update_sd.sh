#!/bin/bash
set -e

# Sizes are in MiB
readonly ROOTFS_FILE="rootfs.tar.gz"

echo "======================================="
echo "=  Chronoptics update sd card script  ="
echo "======================================="

help() {
	bn=`basename $0`
	echo " Usage: $bn <options> device_node"
	echo
	echo " options:"
	echo " -h		display this Help message"
	echo
}

if [[ $EUID -ne 0 ]] ; then
	echo "This script must be run with super-user privileges"
	exit 1
fi

TEMP_DIR=./var_tmp
P1_MOUNT_DIR=${TEMP_DIR}/${FAT_VOLNAME}
P2_MOUNT_DIR=${TEMP_DIR}/rootfs

# Parse command line
moreoptions=1
node="na"
cal_only=0

while [ "$moreoptions" = 1 -a $# -gt 0 ]; do
	case $1 in
	    -h) help; exit 3 ;;
	    -s) cal_only=1 ;;
	    *)  moreoptions=0; node=$1 ;;
	esac
	[ "$moreoptions" = 0 ] && [ $# -gt 1 ] && help && exit 1
	[ "$moreoptions" = 1 ] && shift
done
part=""
if [[ $node == *mmcblk* ]] || [[ $node == *loop* ]] ; then
	part="p"
fi

function format_parts
{
	echo
	echo "Formatting partitions"
	mkfs.ext4 ${node}${part}1 -L rootfs -F
	sync; sleep 1
}

function mount_parts
{
	mkdir -p ${P2_MOUNT_DIR}
	sync
	mount ${node}${part}1 ${P2_MOUNT_DIR}
}

function unmount_parts
{
	umount ${P2_MOUNT_DIR}
	rm -rf ${TEMP_DIR}
}

function install_yocto
{
	echo
	echo "Installing Yocto Root File System"
	pv ${ROOTFS_FILE} | tar -xz -C ${P2_MOUNT_DIR}/
}

umount ${node}${part}*  2> /dev/null || true

format_parts
mount_parts
install_yocto

echo
echo "Syncing"
sync | pv -t

unmount_parts

echo
echo "Done"

exit 0
